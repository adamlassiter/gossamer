#! /usr/bin/env python3

from __future__ import annotations
from collections.abc import Callable
from copy import deepcopy

from abcs import Hash


class KeccakHash(Hash):
    import c_keccak.keccak as cLib

    def __init__(self, rate: int, capacity: int) -> None:
        self.c_hash = self.cLib.keccak_new_hash(rate, capacity)

    def squeeze(self) -> bytes:
        return self.cLib.keccak_squeeze(self.c_hash)

    def __absorb(self, _bytes: bytes):
        self.cLib.keccak_absorb(self.c_hash, _bytes)

    def update(self, text: bytes):
        self.__absorb(text)

    def digest(self) -> bytes:
        return self.squeeze()

    def hexdigest(self) -> str:
        finalised = deepcopy(self)
        digest = finalised.squeeze()
        return digest.hex()

    @classmethod
    def preset(cls, rate: int, capacity: int) -> Callable:
        """
        Returns a factory function for the given bitrate, sponge capacity and output length.
        The function accepts an optional initial input, ala hashlib.
        """
        def create(initial_input: bytes = None):
            h = cls(rate, capacity)
            if initial_input is not None:
                h.update(initial_input)
            return h
        return create


# SHA3 parameter presets
Keccak224 = KeccakHash.preset(1152, 448)
Keccak256 = KeccakHash.preset(1088, 512)
Keccak384 = KeccakHash.preset(832, 768)
Keccak512 = KeccakHash.preset(576, 1024)

