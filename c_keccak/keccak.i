%module keccak

%typemap(in) bytearray* {
    $1 = Keccak_ToBytearray($input);
}
%typemap(out) bytearray* {
    $result = Keccak_FromBytearray($1);
}

%typemap(in) keccak_hash* {
    $1 = Keccak_ToKeccakHash($input);
}
%typemap(out) keccak_hash* {
    $result = Keccak_FromKeccakHash($1);
}

%{
    #define SWIG_FILE_WITH_INIT

    #include "keccak.h"

    extern bytearray *keccak_squeeze(keccak_hash *);
    extern void keccak_absorb(keccak_hash *, bytearray *);
%}

extern bytearray *keccak_squeeze(keccak_hash *);

extern void *keccak_absorb(keccak_hash *, bytearray *);

