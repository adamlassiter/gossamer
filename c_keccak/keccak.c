#include "keccak.h"

#define MIN(a, b) ((a) < (b) ? (a) : (b))


bytearray *Keccak_ToBytearray(PyObject *pyObject) {
    bytearray *ret = (bytearray*) malloc(sizeof(bytearray));
    int err = PyBytes_AsStringAndSize(pyObject, &ret->bytes,  &ret->len);
    if (err == -1) {
        printf("Error: could not parse as bytes object");
        exit(1);
    }
    return ret;
}
PyObject *Keccak_FromBytearray(bytearray *bytes) {
    PyObject *ret = PyBytes_FromStringAndSize(bytes->bytes, bytes->len);
    if (ret == NULL) {
        printf("Error: could not parse as bytes object");
        exit(1);
    }
    return ret;
}


int keccak_LFSR86540(UINT8 *LFSR) {
    int result = ((*LFSR) & 0x01) != 0;
    if (((*LFSR) & 0x80) != 0)
        /* Primitive polynomial over GF(2): x^8+x^6+x^5+x^4+1 */
        (*LFSR) = ((*LFSR) << 1) ^ 0x71;
    else
        (*LFSR) <<= 1;
    return result;
}


void keccak_permute(keccak_hash *hash) {
    UINT8 LFSRstate = 0x01;

    for (unsigned int round = 0; round < 24; round++) {
        {   /* theta-step */
            tKeccakLane C[5], D;
            /* Compute the parity of the columns */
            for (unsigned int x = 0; x < 5; x++)
                C[x] = readLane(hash, x, 0) ^ readLane(hash, x, 1) ^ readLane(hash, x, 2) ^ readLane(hash, x, 3) ^ readLane(hash, x, 4);
            for (unsigned int x = 0; x < 5; x++) {
                /* Compute the θ effect for a given column */
                D = C[(x+4)%5] ^ ROL64(C[(x+1)%5], 1);
                /* Add the θ effect to the whole column */
                for (unsigned int y = 0; y < 5; y++)
                    XORLane(hash, x, y, D);
            }
        }
        {   /* rho-step and pi-step */
            tKeccakLane current, temp;
            /* Start at coordinates (1 0) */
            unsigned int x = 1, y = 0;
            current = readLane(hash, x, y);
            /* Iterate over ((0 1)(2 3))^t * (1 0) for 0 ≤ t ≤ 23 */
            for (unsigned int t = 0; t < 24; t++) {
                /* Compute the rotation constant r = (t+1)(t+2)/2 */
                unsigned int r = ((t + 1) * (t + 2) / 2) % 64;
                /* Compute ((0 1)(2 3)) * (x y) */
                unsigned int Y = (2 * x + 3 * y) % 5; x = y; y = Y;
                /* Swap current and state(x,y), and rotate */
                temp = readLane(hash, x, y);
                writeLane(hash, x, y, ROL64(current, r));
                current = temp;
            }
        }
        {   /* chi-step */
            tKeccakLane temp[5];
            for (unsigned int y = 0; y < 5; y++) {
                /* Take a copy of the plane */
                for (unsigned int x = 0; x < 5; x++)
                    temp[x] = readLane(hash, x, y);
                /* Compute χ on the plane */
                for (unsigned int x = 0; x < 5; x++)
                    writeLane(hash, x, y, temp[x] ^((~temp[(x+1)%5]) & temp[(x+2)%5]));
            }
        }
        {   /* iota-step */
            for (unsigned int j = 0; j < 7; j++) {
                unsigned int bitPosition = (1<<j)-1; /* 2^j-1 */
                if (keccak_LFSR86540(&LFSRstate))
                    XORLane(hash, 0, 0, (tKeccakLane)1<<bitPosition);
            }
        }
    }
}


keccak_hash *new_hash(unsigned int rate, unsigned int capacity) {
    keccak_hash *hash = (keccak_hash*) malloc(sizeof(keccak_hash));
    *hash = (keccak_hash) {
        .state = {0},
        .rate = rate,
        .rate_bytes = rate / 8,
        .block_size = 0,
        .capacity = capacity,
        .digest_size = capacity / 8
    };

    return hash;
}
keccak_hash *keccak_new_hash(unsigned int rate, unsigned int capacity) {
    return new_hash(rate, capacity);
}


void free_hash(keccak_hash *hash) {
    free(hash);
}


bytearray *c_squeeze(keccak_hash *hash) {
    /* Do the padding and switch to the squeezing phase */
    /* Absorb the last few bits and add the first bit of padding (which coincides with the delimiter 0x06) */
    hash->state[hash->block_size] ^= 0x06;
    /* Add the second bit of padding */
    hash->state[hash->rate_bytes-1] ^= 0x80;
    /* Switch to the squeezing phase */
    keccak_permute(hash);

    /* Squeeze out all the output blocks */
    unsigned int digest_size = hash->digest_size;
    bytearray *output = new_bytearray(hash->digest_size);
    char *iter = output->bytes;
    while (digest_size > 0) {
        hash->block_size = MIN(digest_size, hash->rate_bytes);
        memcpy(iter, hash->state, hash->block_size);
        iter += hash->block_size;
        digest_size -= hash->block_size;

        if (digest_size > 0)
            keccak_permute(hash);
    }
    return output;
}
bytearray *keccak_squeeze(keccak_hash *hash) {
    return c_squeeze(hash);
}


void c_absorb(keccak_hash *hash, bytearray *input) {
    /* Absorb all the input blocks */
    while (input->len > 0) {
        hash->block_size = MIN(input->len, hash->rate_bytes);
        for (unsigned int i = 0; i < hash->block_size; i++)
            hash->state[i] ^= input->bytes[i];
        input->bytes += hash->block_size;
        input->len -= hash->block_size;

        if (hash->block_size == hash->rate_bytes) {
            keccak_permute(hash);
            hash->block_size = 0;
        }
    }
}
void keccak_absorb(keccak_hash *hash, bytearray *input) {
    c_absorb(hash, input);
}


bytearray *c_keccak(bytearray *input, int out_bytes) {
    keccak_hash *hash = new_hash(input->len, out_bytes);
    c_absorb(hash, input);
    return c_squeeze(hash);
}

