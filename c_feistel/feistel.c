#include "feistel.h"

#define HASH_BLOCKSIZE 1024
#define FEISTEL_ROUNDS 4
#define byteslen(x) byteslen((char *) x)


bytearray Feistel_ToBytearray(PyObject *pyObject) {
    bytearray ret;
    int err = PyBytes_AsStringAndSize(pyObject, &ret.bytes,  &ret.len);
    if (err == -1) {
        printf("Error: could not parse as bytes object");
        exit(1);
    }
    return ret;
}
PyObject *Feistel_FromBytearray(bytearray *bytes) {
    PyObject *ret = PyBytes_FromStringAndSize(bytes->bytes, bytes->len);
    if (ret == NULL) {
        printf("Error: could not parse as bytes object");
        exit(1);
    }
    return ret;
}


void xor (bytearray *a, bytearray *b, bytearray *o) {
    for (unsigned int i = 0; i < o->len; i++)
        o->bytes[i] = a->bytes[i] ^ b->bytes[i];
}


bytearray *feistel_hash_block(bytearray *input, bytearray *key, bytearray *output, hash_func hash, bool forward) {
    assert(input->len == HASH_BLOCKSIZE);
    bytearray *buffer = new_bytearray(2 * input->len);
    bytearray left, right, *in, *out, hashed;
    memcpy(output->bytes, input->bytes, input->len);
    for (int round_nmbr = forward? 0 : FEISTEL_ROUNDS - 1;
         forward? round_nmbr < FEISTEL_ROUNDS : round_nmbr>=0;
         forward? round_nmbr++ : round_nmbr--) {
        left = (bytearray) {
            .bytes = output->bytes,
            .len = output->len / 2
        };
        right = (bytearray) {
            .bytes = output->bytes + left->len,
            .len = left->len
        };
        in = round_nmbr % 2 ? &left : &right;
        out = round_nmbr % 2 ? &right : &left;

        memcpy(buffer->bytes, key->bytes, key->len);
        memcpy(buffer->bytes + key->len, in->bytes, in->len);
        buffer->len = key->len + in->len;
        hashed = hash(buffer, HASH_BLOCKSIZE);
        xor(hashed, *out, *out);
        free(hashed->bytes);
    }
    return output;
}


bytearray *feistel_encrypt(bytearray *plaintext, bytearray *key, bytearray *iv, int mode) {
    assert(plaintext->len % HASH_BLOCKSIZE == 0);
    bytearray *ciphertext = new_bytearray(plaintext->len), *temp = new_bytearray(HASH_BLOCKSIZE);
    bytearray sub_text;
    for (unsigned int i = 0; i < plaintext->len / HASH_BLOCKSIZE; i++) {
        sub_text = (bytearray) {
            .bytes = plaintext.bytes + i * plaintext.len / HASH_BLOCKSIZE,
            .len = HASH_BLOCKSIZE
        };

        char *dest = ciphertext->bytes + i * HASH_BLOCKSIZE;

        switch ((enum cipher_mode) mode) {
            case ECB:
                feistel_hash_block(sub_text, key, temp, c_keccak, true);
                memcpy(dest, temp->bytes, HASH_BLOCKSIZE);
                break;

            case CBC:
                xor(iv, sub_text, iv);
                feistel_hash_block(iv, key, temp, c_keccak, true);
                memcpy(dest, temp->bytes, HASH_BLOCKSIZE);
                break;

            case PCBC:
                xor(iv, sub_text, iv);
                feistel_hash_block(iv, key, temp, c_keccak, true);
                memcpy(dest, temp->bytes, HASH_BLOCKSIZE);
                xor(temp, sub_text, iv);
                break;

            case CFB:
                feistel_hash_block(iv, key, temp, c_keccak, true);
                xor(sub_text, temp, iv);
                memcpy(dest, iv->bytes, HASH_BLOCKSIZE);
                break;

            case OFB:
                feistel_hash_block(iv, key, temp, c_keccak, true);
                memcpy(iv->bytes, temp->bytes, HASH_BLOCKSIZE);
                xor(sub_text, temp, temp);
                memcpy(dest, temp->bytes, HASH_BLOCKSIZE);
                break;
        }
    }
    free_bytearray(temp);
    return ciphertext;
}


bytearray *feistel_decrypt(bytearray *ciphertext, bytearray *key, bytearray *iv, int mode) {
    assert(ciphertext->len % HASH_BLOCKSIZE == 0);
    bytearray *plaintext = new_bytearray(ciphertext->len), temp = new_bytearray(HASH_BLOCKSIZE);
    bytearray sub_text;
    for (unsigned int i = 0; i < ciphertext.len / HASH_BLOCKSIZE; i++) {
        sub_text = (bytearray) {
            .bytes = ciphertext->bytes + i * ciphertext->len / HASH_BLOCKSIZE,
            .len = HASH_BLOCKSIZE
        };

        switch ((enum cipher_mode) mode) {
            case ECB:
                feistel_hash_block(sub_text, key, temp, c_keccak, false);
                memcpy(plaintext->bytes + i * HASH_BLOCKSIZE, temp->bytes, HASH_BLOCKSIZE);
                break;

            case CBC:
                feistel_hash_block(sub_text, key, temp, c_keccak, false);
                xor(iv, temp, temp);
                memcpy(iv->bytes, sub_text->bytes, HASH_BLOCKSIZE);
                memcpy(plaintext->bytes + i * HASH_BLOCKSIZE, temp->bytes, HASH_BLOCKSIZE);
                break;

            case PCBC:
                feistel_hash_block(sub_text, key, temp, c_keccak, false);
                xor(iv, temp, temp);
                xor(sub_text, temp, iv);
                memcpy(plaintext->bytes + i * HASH_BLOCKSIZE, temp->bytes, HASH_BLOCKSIZE);
                break;

            case CFB:
                feistel_hash_block(iv, key, temp, c_keccak, true);
                xor(sub_text, temp, iv);
                memcpy(plaintext->bytes + i * HASH_BLOCKSIZE, iv->bytes, HASH_BLOCKSIZE);
                memcpy(iv->bytes, sub_text.bytes, HASH_BLOCKSIZE);
                break;

            case OFB:
                feistel_hash_block(iv, key, temp, c_keccak, true);
                memcpy(iv->bytes, temp->bytes, HASH_BLOCKSIZE);
                xor(sub_text, temp, temp);
                memcpy(plaintext->bytes + i * HASH_BLOCKSIZE, temp->bytes, HASH_BLOCKSIZE);
                break;
        }
    }
    free_bytearray(temp);
    return plaintext;
}

