%module feistel

%typemap(in) bytearray* {
    $1 = Feistel_ToBytearray($input);
}
%typemap(out) bytearray* {
    $result = Feistel_FromBytearray($1);
}

%{
    #define SWIG_FILE_WITH_INIT

    #include "feistel.h"
    #include "../c_keccak/keccak.h"
    #include "../c_common/bytearray.h"

    extern bytearray *feistel_encrypt(bytearray *, bytearray *, bytearray *, int);
    extern bytearray *feistel_decrypt(bytearray *, bytearray *, bytearray *, int);
%}

extern bytearray feistel_encrypt(bytearray *, bytearray *, bytearray *, int);
extern bytearray feistel_decrypt(bytearray *, bytearray *, bytearray *, int);

