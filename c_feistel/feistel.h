#include "../c_keccak/keccak.h"
#include "../c_common/bytearray.h"
// FIXME: Is it bad to include relative (cousin) directories

#include <Python.h>

#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#ifndef FEISTEL_H
    #define FEISTEL_H

    enum cipher_mode {
        ECB, CBC, PCBC, CFB, OFB
    } cipher_mode;

    typedef void (*round_func)(bytearray *, bytearray *, bytearray *);
    typedef bytearray (*hash_func)(bytearray *, int);

    bytearray Feistel_ToBytearray(PyObject *);
    PyObject *Feistel_FromBytearray(bytearray *);

    bytearray feistel_encrypt(bytearray *, bytearray *, bytearray *, int);
    bytearray feistel_decrypt(bytearray *, bytearray *, bytearray *, int);

#endif /* end of include guard: FEISTEL_H */

