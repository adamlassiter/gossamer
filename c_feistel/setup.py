#! /usr/bin/python3

from distutils.core import setup, Extension

feistel_module = Extension('_feistel',
                           sources=['feistel_wrap.c', 'feistel.c',
                                    '../c_keccak/keccak.c', '../c_common/bytearray.c'],
                           extra_compile_args=['-g'])

setup(name='feistel',
      version='0.1',
      ext_modules=[feistel_module],
      py_modules=['feistel'])
