#ifndef BYTEARRAY_H
    #define BYTEARRAY_H

    typedef struct {
        char *bytes;
        long int len;
    } bytearray;

    bytearray *new_bytearray(int);

    void free_bytearray(bytearray *);

#endif /* end of include guard: BYTEARRAY_H */

