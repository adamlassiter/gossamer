#include "bytearray.h"

#include <stdlib.h>

bytearray *new_bytearray(int len) {
    bytearray *b = (bytearray*) malloc(sizeof(bytearray));
    *b = (bytearray) {
        .len = (long int) len,
        .bytes = calloc(sizeof(b.bytes), len)
    };
    return b;
}


void free_bytearray(bytearray b) {
    free(b.bytes);
    free(b);
}

