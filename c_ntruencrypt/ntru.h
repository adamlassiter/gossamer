#include <Python.h>
#include <string.h>
#include <stdlib.h>

#ifndef NTRU_H
    #define NTRU_H

    #define order(p) (p.len - 1)
    #define mod(a, b) ((b + (a % b)) % b)
    #define true 1

    typedef struct {
        int len;
        int   *coeffs;
    } polynomial;

    polynomial *Ntru_ToPolynomial(PyObject *);
    PyObject *Ntru_FromPolynomial(polynomial *);

    polynomial *ntru_rshift(polynomial *, int);
    polynomial *ntru_lshift(polynomial *, int);

    int         ntru_degree(polynomial *);
    polynomial *ntru_centerlift(polynomial *, int);

    polynomial *ntru_s_mul(polynomial *, int);
    polynomial *ntru_s_add(polynomial *, int);
    polynomial *ntru_s_mod(polynomial *, int);

    polynomial *ntru_v_add(polynomial *, polynomial *);
    polynomial *ntru_v_sub(polynomial *, polynomial *);
    polynomial *ntru_v_mul(polynomial *, polynomial *);

    polynomial *ntru_inverse_modp(polynomial *, int);
    polynomial *ntru_inverse_modpn(polynomial *, int);

#endif /* end of include guard: NTRU_H  */

