%module ntru

%typemap(in) polynomial* {
    $1 = Ntru_ToPolynomial($input);
}
%typemap(out) polynomial* {
    $result = Ntru_FromPolynomial($1);
}

%{
    #define SWIG_FILE_WITH_INIT

    #include "ntru.h"

    extern polynomial *ntru_rshift(polynomial *, int);
    extern polynomial *ntru_lshift(polynomial *, int);

    extern int         ntru_degree(polynomial *);
    extern polynomial *ntru_centerlift(polynomial *, int);

    extern polynomial *ntru_s_mul(polynomial *, int);
    extern polynomial *ntru_s_add(polynomial *, int);
    extern polynomial *ntru_s_mod(polynomial *, int);

    extern polynomial *ntru_v_add(polynomial *, polynomial *);
    extern polynomial *ntru_v_sub(polynomial *, polynomial *);
    extern polynomial *ntru_v_mul(polynomial *, polynomial *);

    extern polynomial *ntru_inverse_modp(polynomial *, int);
    extern polynomial *ntru_inverse_modpn(polynomial *, int);
%}

extern polynomial *ntru_rshift(polynomial *, int);
extern polynomial *ntru_lshift(polynomial *, int);

extern int         ntru_degree(polynomial *);
extern polynomial *ntru_centerlift(polynomial *, int);

extern polynomial *ntru_s_mul(polynomial *, int);
extern polynomial *ntru_s_add(polynomial *, int);
extern polynomial *ntru_s_mod(polynomial *, int);

extern polynomial *ntru_v_add(polynomial *, polynomial *);
extern polynomial *ntru_v_sub(polynomial *, polynomial *);
extern polynomial *ntru_v_mul(polynomial *, polynomial *);

extern polynomial *ntru_inverse_modp(polynomial *, int);
extern polynomial *ntru_inverse_modpn(polynomial *, int);

