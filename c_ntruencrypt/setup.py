#! /usr/bin/python3

from distutils.core import setup, Extension

ntru_module = Extension('_ntru',
                        sources=['ntru_wrap.c', 'ntru.c'],
                        extra_compile_args=['-g'])

setup(name='ntru',
      version='0.1',
      ext_modules=[ntru_module],
      py_modules=['ntru'])
